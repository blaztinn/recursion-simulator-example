#![feature(generators, generator_trait)]
#![feature(test)]

extern crate test;

use std::cell::RefCell;
use std::env;
use std::ops::Generator;
use std::ops::GeneratorState;

// Recursive function that we want to change to a stack-based recursion
fn count(n: u32) -> u32 {
    match n {
        0 => 0,
        1 => 1, // This line is not needed but it seems to prevent compile-time evaluation
        _ => count(n - 1) + 1,
    }
}

// Macro to make a "recursive call" inside the generator. Yields the parameters
// for a recursive call and retrieves the ret_val when the generator is resumed.
macro_rules! rec_call {
    ($ret_val:ident, $arg:expr) => {
        ({
            yield $arg;
            let r = *$ret_val.borrow(); // Bind to a variable so that the borrow ends here
            r
        })
    };
}

// Transformed recursive function to work with our recursion executor.
// `ret_val` is used to retrieve the return value of the recursive calls.
// Returns a generator that will make recursive calls and keep the state.
// NOTE: We could create some macro here to make this nicer though.
fn gen_count<'r>(
    ret_val: &'r RefCell<u32>,
    n: u32,
) -> impl Generator<Yield = u32, Return = u32> + 'r {
    move || match n {
        0 => 0,
        1 => 1,
        _ => rec_call!(ret_val, n - 1) + 1,
    }
}

// Generic executor of the recursive calls using a stack.
fn rec_execute_inner<'r, A, R, F, G>(ret_val: &'r RefCell<R>, func: &F, arg: A)
where
    G: Generator<Yield = A, Return = R>,
    F: Fn(&'r RefCell<R>, A) -> G,
{
    let mut callstack = vec![];
    callstack.push(func(&ret_val, arg));
    //use std::mem;
    //println!("Stack element size: {}", mem::size_of_val(&callstack[0]));
    while !callstack.is_empty() {
        let gen = callstack.last_mut().unwrap();
        let ret = unsafe { gen.resume() };
        match ret {
            GeneratorState::Yielded(arg) => callstack.push(func(&ret_val, arg)),
            GeneratorState::Complete(ret) => {
                ret_val.replace(ret);
                callstack.pop();
            }
        }
    }
}

// Create the ret_val value, execute the recursive calls with a reference to it
// and return the result.
// NOTE: Couldn't find a way to make this compile, used the bottom macro instead
#[allow(dead_code)]
fn rec_execute<A, R, F, G>(func: &F, arg: A) -> R
where
    R: Default,
    G: Generator<Yield = A, Return = R>,
    F: for<'r> Fn(&'r RefCell<R>, A) -> G,
{
    let ret_val = RefCell::new(Default::default());
    rec_execute_inner(&ret_val, func, arg);
    ret_val.into_inner()
}

// Create the ret_val value, execute the recursive calls with a reference to it
// and return the result.
macro_rules! rec_execute {
    ($func:expr, $arg:expr) => {{
        let ret_val = RefCell::new(Default::default());
        rec_execute_inner(&ret_val, $func, $arg);
        ret_val.into_inner()
    }};
}

// Usage:
//    ./rec normal 1000
//    ./rec simulate 1000
fn main() {
    let use_normal: bool = env::args().nth(1).unwrap_or("simulate".into()) == "normal";
    let arg: u32 = env::args().nth(2).unwrap_or("20".into()).parse().unwrap();
    if use_normal {
        let result = count(arg);
        println!("result[normal]: {}", result);
    } else {
        let result = rec_execute!(&gen_count, arg);
        println!("result[simulate]: {}", result);
    };
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test::Bencher;

    #[test]
    fn test_normal() {
        assert_eq!(11, count(11));
        assert_eq!(12, count(12));
    }

    #[test]
    fn test_rec() {
        assert_eq!(11, rec_execute!(&gen_count, 11));
        assert_eq!(12, rec_execute!(&gen_count, 12));
    }

    const SMALL: u32 = 500;
    const MEDIUM: u32 = 50_000;
    const LARGE: u32 = 500_000;

    #[bench]
    fn bench_small_normal(b: &mut Bencher) {
        b.iter(|| {
            let n = test::black_box(SMALL);
            count(n)
        });
    }

    #[bench]
    fn bench_small_rec(b: &mut Bencher) {
        b.iter(|| {
            let n = test::black_box(SMALL);
            rec_execute!(&gen_count, n)
        });
    }

    #[bench]
    fn bench_medium_normal(b: &mut Bencher) {
        b.iter(|| {
            let n = test::black_box(MEDIUM);
            count(n)
        });
    }

    #[bench]
    fn bench_medium_rec(b: &mut Bencher) {
        b.iter(|| {
            let n = test::black_box(MEDIUM);
            rec_execute!(&gen_count, n)
        });
    }

    #[bench]
    fn bench_large_normal(b: &mut Bencher) {
        b.iter(|| {
            let n = test::black_box(LARGE);
            count(n)
        });
    }

    #[bench]
    fn bench_large_rec(b: &mut Bencher) {
        b.iter(|| {
            let n = test::black_box(LARGE);
            rec_execute!(&gen_count, n)
        });
    }
}
